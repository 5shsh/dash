#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE_NAMESPACE=library
ARG BASE_IMAGE_NAME=alpine
ARG BASE_IMAGE_VERSION=3.12

FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION} as builder

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
RUN apk add --no-cache py3-pip

COPY root/requirements.txt /requirements.txt
RUN pip install -r requirements.txt

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#
# -------
#
FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
COPY root /
COPY app /app

COPY --from=builder /root/.cache /root/.cache

RUN apk add --no-cache py3-pip scapy tcpdump && \
    pip install -r requirements.txt && rm -rf /root/.cache

ENTRYPOINT [ "/entrypoint.sh" ]

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL   org.label-schema.name="homesmarthome/dash" \
        org.label-schema.description="Amazon Dash listener" \
        org.label-schema.url="homesmarthome.5square.de" \
        org.label-schema.vcs-ref="$VCS_REF" \
        org.label-schema.vcs-url="$VCS_URL" \
        org.label-schema.vendor="fvsqr" \
        org.label-schema.version=$VERSION \
        org.label-schema.build-date=$BUILD_DATE \
        org.label-schema.schema-version="1.0" \
        org.label-schema.docker.cmd="TBD"