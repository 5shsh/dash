import paho.mqtt.client as mqtt
import os
import threading
from datetime import datetime
from datetime import timedelta
import scapy.all as scapy

last_press = datetime.now() - timedelta(seconds=10)

MQTT_HOST = os.environ['MQTT_HOST']
DASH_LIST = os.environ['DASH_LIST']

import pprint

def arp_received(packet):
    if packet['ARP'].op == 1 and packet['ARP'].hwdst == '00:00:00:00:00:00':
        if packet['ARP'].hwsrc in DASH_LIST :  # This is the MAC of the first dash button
            global last_press
            global client
            now = datetime.now()
            if last_press + timedelta(seconds=5) <= now:
                print("Button pressed: "+packet['ARP'].hwsrc)
                last_press = now
                topic = "dash/" + packet['ARP'].hwsrc + "/pushed"
                payload = "{\"val\":1}"
                client.publish(topic, payload)

if __name__ == "__main__":
    client = mqtt.Client()
    client.connect(MQTT_HOST, 1883, 60)
    ct = threading.Thread(target=client.loop_forever)
    ct.daemon = True
    ct.start()

    print("Listening for ARP packets...")
    scapy.sniff(prn=arp_received, iface="eth0", filter="arp", store=0, count=0)